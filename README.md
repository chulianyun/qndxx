# 初恋云-湖南青年大学习自动学习（php脚本）

#### 介绍
本人大学生，日常周一大学习，很烦得，还被催，就写了这个
自动学习+图片获取

#### 软件架构
软件架构说明


#### 安装教程

1.  下载你需要得文件，直接上传主机或者服务器即可

#### 使用说明

1.  获取图片无需参数
2.  一键学习需要参数cookie
3.  细分得需要输入type参数

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
